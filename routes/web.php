<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/submit-form', 'HomeController@submit')->name('submit');
Route::get('/get-date', 'HomeController@getDate');
Route::get('/get-timeslot', 'HomeController@getTimeslot');
Route::get('/submit-booking', 'HomeController@submitBooking')->name('submit_booking');
Route::get('/testing/{code}', 'HomeController@test');
Route::get('/test-email', 'HomeController@testEmail');
Route::get('/privacy-policy', 'HomeController@viewPolicy')->name('privacy-policy');
Route::get('/terms-and-conditions', 'HomeController@viewTnc')->name('terms-and-conditions');
Route::get('/makeup-terms-and-conditions', 'HomeController@viewMakeupTnc')->name('makeup-terms-and-conditions');

Route::get('/redeem-sample-kit/{id}/{code}', 'HomeController@redeem')->name('redeem');
// Route::get('/redeem-makeup/{timeslot}/{id}', 'HomeController@redeemMakeup')->name('redeem-makeup');
Route::get('/thank-you/{code}', 'HomeController@thankYou')->name('thank-you');
Route::get('/thank-you-booking/{timeslot}', 'HomeController@thankYouBooking')->name('thank-you-booking');

Route::get('/redirect/{target}', 'HomeController@redirect')->name('redirect');

Route::prefix('admin')->group(function () {
    Route::get('/', function () {return redirect()->route('admin-dashboard');});
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/dashboard', 'AdminController@index')->name('admin-dashboard');
    Route::get('/resend-email/{code}', 'AdminController@resendEmail')->name('resend-email');
    Route::get('/cancel-booking/{id}', 'AdminController@cancelBooking')->name('cancel-booking');
    Route::get('/export-registration-list', 'AdminController@exportRegistrationList')->name('export-registration-list');
    Route::get('/export-booking-list', 'AdminController@exportBookingList')->name('export-booking-list');
    Route::get('/export-total-count', 'AdminController@exportTotalCount')->name('export-total-count');
    Route::prefix('datatables')->group(function () {
        Route::get('/total-registration', 'AdminController@datatablesTotalRegistration')->name('datatables-total-registration');
        Route::get('/registration-list', 'AdminController@datatablesRegistrationList')->name('datatables-registration-list');
        Route::get('/booking-list', 'AdminController@datatablesBookingList')->name('datatables-booking-list');
    });
});
