<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Timeslot extends Model
{
    protected $fillable=[
       'artist_id', 'date', 'time',
   	];

   	public function artists(){
   		return $this->belongsTo('App\Artist', 'artist_id', 'id');
   	}
}
