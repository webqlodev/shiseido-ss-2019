<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Excel;
use Datatables;
use App\Mail\ThankYouForRegistering;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Makeup_booking;
use App\Timeslot;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function datatablesTotalRegistration()
    {
        $total = new Collection;
        $startDate = new Carbon( env('START_TIME') );
        $endDate = new Carbon( env('END_TIME') );

        // Loop through registrations table by date

        $date = $startDate;

        while ( $date->lte($endDate) && !$date->isTomorrow() ) {
            $date_current =$date->format('j F Y (l)');
            $date_from = $date->toDateString();
            $date_to = $date->addDay()->toDateString();
            $total_registrations = DB::table('registrations')->whereBetween( 'created_at', [$date_from,$date_to])->count();
            $total_redeem = DB::table('registrations')->whereBetween( 'updated_at', [$date_from,$date_to])->where('redeemed', 1)->count();
            $total->push([
                'date' => $date_current,
                'total' => $total_registrations,
                'total_redeem' => $total_redeem
            ]);
        }
        $total->reverse();
        return Datatables::of($total)->make(true);
    }

    public function datatablesRegistrationList()
    {
        $registrations = DB::table('registrations')->select([
            'id',
            'fullname',
            'email',
            'phone',
            'unique_code as code',
            'redeem_location as location',
            'created_at as time',
            'updated_at as time_redeem',
        ])->orderBy('created_at', 'desc')->get();

        $registrations->transform(function ($item, $key) {
            $time = new Carbon($item->time);
            $time_redeem = is_null($item->time_redeem) ? "Yet to redeem." : new Carbon($item->time_redeem);
            $item->time = $time->format('j F Y (l)');
            $item->time_redeem = is_null($item->time_redeem) ? $time_redeem : $time_redeem->format('j F Y (l)');
            return $item;
        });

        return Datatables::of($registrations)->make(true);
    }

    public function datatablesBookingList()
    {   
        $bookings = DB::table('makeup_bookings')
            ->join('timeslots','timeslots.id','=','makeup_bookings.time_slot_id')
            ->leftJoin('artists','artists.id','=','timeslots.artist_id')
            ->select(['makeup_bookings.*','artists.name as artist','timeslots.date as date','timeslots.time as time'])
            ->orderBy('date', 'asc')
            ->get();
        $bookings->transform(function ($item, $key) {
            $time_redeem = is_null($item->updated_at) ? "Yet to redeem." : new Carbon($item->updated_at);
            $item->updated_at = is_null($item->updated_at) ? $time_redeem : $time_redeem->format('j F Y (l)');
            return $item;
        });

        return Datatables::of($bookings)->make(true);
    }

    public function cancelBooking($id, Request $request){
        $booking = Makeup_booking::find($id);
        if( $booking->time_slot_id !== 0 ){
            $timeslot = Timeslot::find($booking->time_slot_id);
            $booking->cancelled_time_slot = $booking->time_slot_id;
            $booking->cancelled_email = $booking->email;
            $booking->cancelled_phone = $booking->phone;
            $booking->email = 'cancel email order'.$booking->id;
            $booking->phone = 'cancel phone order'.$booking->id;
            $timeslot->status = 0;
            $timeslot->save();
            $booking->save();
            return redirect()->back();
        }else{
            return abort(404);
        }
        
    }

    public function resendEmail($code)
    {
        Mail::to( DB::table('registrations')->where('unique_code', $code)->value('email') )->send( new ThankYouForRegistering($code) );

        return 'Email sent. <button onclick="window.close()">Close</button>';
    }

    public function exportBookingList(){
        $fileName = 'booking_makeup_list@' . Carbon::now();
        Excel::create($fileName, function ($excel) {
            // left align all rows
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('Makeup_booking', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Full Name',
                    'Phone Number',
                    'Email Address',
                    'Makeup Artist',
                    'Date',
                    'Time',
                    'Status',
                ]);

                $bookings = DB::table('makeup_bookings')
                    ->join('timeslots','timeslots.id','=','makeup_bookings.time_slot_id')
                    ->leftJoin('artists','artists.id','=','timeslots.artist_id')
                    ->select(['makeup_bookings.*','artists.name as artist','timeslots.date as date','timeslots.time as time'])
                    ->orderBy('date', 'asc')
                    ->get();

                foreach ($bookings as $key => $value) {
                    $rowIndex++;
                    $time_redeem = is_null($value->updated_at) ? "Yet to redeem." : new Carbon($value->updated_at);
                    $sheet->row($rowIndex, [
                        $value->fullname,
                        !is_null($value->cancelled_phone) ? $value->cancelled_phone : $value->phone,
                        !is_null($value->cancelled_email) ? $value->cancelled_email : $value->email,
                        $value->artist,
                        $value->date,
                        $value->time,
                        $value->cancelled_time_slot == 0  ? "Valid" : "Cancelled Booking",
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }

    public function exportRegistrationList()
    {
        $fileName = env('APP_NAME') . ' @ ' . Carbon::now();

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('registrations', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Full Name',
                    'Register Date',
                    'Email Address',
                    'Phone Number',
                    'Redeem Location',
                    'Redeemed Date',
                ]);

                $registrations = DB::table('registrations')->orderBy('id', 'asc')->get();

                foreach ($registrations as $key => $value) {
                    $rowIndex++;
                    $time = new Carbon($value->created_at);
                    $time_redeem = is_null($value->updated_at) ? "Yet to redeem." : new Carbon($value->updated_at);
                    $sheet->row($rowIndex, [
                        $value->fullname,
                        $time->format('j F Y (l)'),
                        $value->email,
                        $value->phone,
                        $value->redeem_location,
                        is_null($value->updated_at) ? $time_redeem : $time_redeem->format('j F Y (l)'),
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }

    public function exportTotalCount()
    {
        $fileName = env('APP_NAME') . '_total_count @ ' . Carbon::now();

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('registrations', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Date',
                    'Total Registration',
                    'Total Redeemed',
                ]);

                $total = new Collection;
                $startDate = new Carbon( env('START_TIME') );
                $endDate = new Carbon( env('END_TIME') );
                // Loop through registrations table by date
                $date = $startDate;

                while ( $date->lte($endDate) && !$date->isTomorrow() ) {
                    $date_current =$date->format('j F Y (l)');
                    $date_from = $date->toDateString();
                    $date_to = $date->addDay()->toDateString();
                    $total_registrations = DB::table('registrations')->whereBetween( 'created_at', [$date_from,$date_to])->count();
                    $total_redeem = DB::table('registrations')->whereBetween( 'updated_at', [$date_from,$date_to])->where('redeemed', 1)->count();
                    $total->push([
                        'date' => $date_current,
                        'total' => $total_registrations,
                        'total_redeem' => $total_redeem
                    ]);
                }
                foreach ($total as $value) {
                    $rowIndex++;
                    $sheet->row($rowIndex, [
                        $value['date'],
                        $value['total'],
                        $value['total_redeem'],
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }
}