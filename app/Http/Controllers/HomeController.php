<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Validator;
use App\Mail\ThankYouForRegistering;
use App\Mail\ThankYouForBooking;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Makeup_booking;
use App\Timeslot;

class HomeController extends Controller
{
    /**
     * Show the application home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timeslots = Timeslot::where('status', 0)->get();
        $artists_name = array(
            1 => "Nannan Shindo",
            2 => "Emmy Agung",
            3 => "Kenny Yee",
            4 => "Shiyo Joo",
            5 => "Cat Yong",
            6 => "Ummi Nasir",
            7 => "Gary Cheok"
         );
        $artist_name_array = array();
        foreach ($timeslots as $timeslot) {
            if( !in_array($timeslot->artists->name,$artist_name_array) ){
                $artist_name_array[$timeslot->artists->id] = $timeslot->artists->name;
            }
        }
        return view('home', ['artist_name_array' => $artist_name_array, 'artists_name' => $artists_name] );
    }

    public function submit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:registrations,email',
            'phone' => 'required|regex:/\d+/|max:20|unique:registrations,phone',
            'location' => 'required',
            'tnc' => 'accepted',
        ], [
            'fullname.required' => 'Name is required to participate.',
            'fullname.max' => 'Name is too long.',
            'email.required' => 'Email address is required to participate.',
            'email.email' => 'Email address must be valid.',
            'email.max' => 'Email address is too long.',
            'email.unique' => 'This email address has already been registered.',
            'phone.required' => 'Phone number is required to participate.',
            'phone.regex' => 'Only contains numbers. Plus signs, dashes, and letters are not allowed.',
            'phone.max' => 'Phone number should not exceed 20 characters.',
            'phone.unique' => 'This phone number has already been registered.',
            'location.required' => 'Location must be chosen.',
            'tnc.accepted' => 'Policy must be accepted.',
        ]);

        if ( $validator->fails() ) {
            return response()->json($validator->errors());
        }else{
           // Generate code and create voucher

            $code = $this->generateUniqueCode();

            // Create new registration
            DB::table('registrations')->insert([
                'fullname' => $request->fullname,
                'email' => $request->email,
                'phone' => $request->phone,
                'unique_code' => $code,
                'redeem_location' => $request->location,
                'receive_updates' => $request->receive_update,
                'created_at' => Carbon::now(),
            ]); 
            HomeController::makeVoucher($code);
            Mail::to( $request->email )->send( new ThankYouForRegistering($code) );
            return response()->json(['success'=>$code]);
        }
    }

    public function submitBooking(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'roadshow_fullname' => 'required|max:255',
            'roadshow_email' => 'required|email|max:255|unique:makeup_bookings,email',
            'roadshow_phone' => 'required|regex:/\d+/|max:20|unique:makeup_bookings,phone',
            'roadshow_artist' => 'required',
            'roadshow_date' => 'required',
            'roadshow_time' => 'required',
            'roadshow_tnc' => 'accepted',
        ], [
            'roadshow_fullname.required' => 'Name is required to participate.',
            'roadshow_fullname.max' => 'Name is too long.',
            'roadshow_email.required' => 'Email address is required to participate.',
            'roadshow_email.email' => 'Email address must be valid.',
            'roadshow_email.max' => 'Email address is too long.',
            'roadshow_email.unique' => 'This email address has already been registered.',
            'roadshow_phone.required' => 'Phone number is required to participate.',
            'roadshow_phone.regex' => 'Only contains numbers. Plus signs, dashes, and letters are not allowed.',
            'roadshow_phone.max' => 'Phone number should not exceed 20 characters.',
            'roadshow_phone.unique' => 'This phone number has already been registered.',
            'roadshow_artist.required' => 'Artist must be chosen.',
            'roadshow_date.required' => 'Date must be chosen.',
            'roadshow_time.required' => 'Time must be chosen.',
            'roadshow_tnc.accepted' => 'Policy must be accepted.',
        ]);

        if ( $validator->fails() ) {
            return response()->json($validator->errors());
        }else{
            $check_timeslot_valid = Timeslot::find($request->roadshow_time);
            if( $check_timeslot_valid->status === 0 ){

                $code = $this->generateUniqueCode();
                // Create new booking
                Makeup_booking::insert([
                    'fullname' => $request->roadshow_fullname,
                    'email' => $request->roadshow_email,
                    'phone' => $request->roadshow_phone,
                    'time_slot_id' => $request->roadshow_time,
                    'image_code' => $code,
                    'created_at' => Carbon::now(),
                ]);

                // Update timeslot status
                $check_timeslot_valid->status = 1;
                $check_timeslot_valid->save();
                HomeController::makeBookingVoucher($code ,$request->roadshow_time);
                Mail::to( $request->roadshow_email )->send( new ThankYouForBooking($code) );
                return response()->json([ 'success'=> $code ]);
            }
            else{
                return response()->json(['failed'=>'Current timeslot has been booked by others!']);
            }
        }
    }

    public function getDate(Request $request)
    {
        $dates_array = array();
        $timeslots = Timeslot::where('artist_id', $request['artist_id'])->where('status', 0)->get();
        $html_code = "<option value=''>Please choose one</option>";
        foreach ($timeslots as $timeslot) {
            if( !in_array($timeslot->date, $dates_array) ){
                $dates_array[] =  $timeslot->date; 
                $html_code .= "<option>".$timeslot->date."</option>";
            }
        }
        return response()->json(['success' => $html_code]);
    }

    public function getTimeslot(Request $request)
    {
        $timeslots_array = array();
        $timeslots = Timeslot::where('artist_id', $request['artist_id'])->where('date', $request['roadshow_date'])->get();
        $html_code = "<option value=''>Please choose one</option>";
        foreach ($timeslots as $timeslot) {
            if( !in_array($timeslot->time, $timeslots_array) ){
                $timeslots_array[] =  $timeslot->time; 
                if( $timeslot->status == 0 ){
                    $html_code .= "<option value='".$timeslot->id."'>".$timeslot->time."</option>";
                }else{
                    $html_code .= "<option value='".$timeslot->id."' disabled>".$timeslot->time."</option>";
                }                
            }
        }
        return response()->json(['success' => $html_code]);
    }

    public function thankYou($code)
    {
        $registration = DB::table('registrations')->where('unique_code', $code)->first();
        // Send email
        if ($registration) {
            return view('thank-you', ['registration' => $registration]);
        } else {
            abort(404);
        }
    }

    public function thankYouBooking($code)
    {
        $booking = DB::table('makeup_bookings')->where('image_code', $code)->where('time_slot_id','!=', 0 )->first();
        // Send email
        if ( $booking ) {
            return view('thank-you-booking');
        } else {
            abort(404);
        }
    }

    public function redeem($id,$code){
        $registration = DB::table('registrations')->where([['unique_code', '=', $code],['id','=', $id]])->first();
        $is_valid = 0;
        if( $registration->redeemed !== 0 ){
            $is_valid = 1;
            return view('redeem', ['is_valid' => $is_valid]);
        }
        DB::table('registrations')->where('id', $id)->update([
            'redeemed'   => 1,
            'updated_at' => Carbon::now()
        ]);
        return view('redeem',['is_valid' => $is_valid]);
    }

    public function redeemMakeup($timeslot,$id){
        $booking = Makeup_booking::where([['time_slot_id', '=', $timeslot],['id','=', $id]])->first();
        $is_valid = 0;
        if( $booking ){
            if( $booking->redeemed !== 0 || $booking->time_slot_id == 0 ){
                $is_valid = 1;
                return view('redeem', ['is_valid' => $is_valid]);
            }
            $booking->redeemed = 1;
            $booking->save();
            return view('redeem',['is_valid' => $is_valid]);
        }else{
            return abort(404);
        }      
    }

    public function redirect($target)
    {
        DB::table('clicks')->insert([
            'target' => $target,
            'created_at' => Carbon::now(),
        ]);

        switch ($target) {
            case 'facebook':
            case 'facebook-email':
                $url = 'https://www.facebook.com/ShiseidoMalaysia/';
                break;
            case 'youtube':
            case 'youtube-email':
                $url = 'https://www.youtube.com/user/OfficialShiseidoMsia';
                break;
            case 'homelink':
                $url = 'https://www.shiseido.com.my/';
                break;
            case 'shiseido-ss-product-foundation':
                $url = 'https://www.shiseido.com.my/shiseido-synchro-skin-self-refreshing-foundation-1011607220SHI.html';
                break;
            case 'shiseido-ss-product-concealer':
                $url = 'https://www.shiseido.com.my/synchro-skin-self-refreshing-dual-tip-concealer-1011572710SHI.html?dwvar_1011572710SHI_color=C1011573010&cgid=M1_Face';
                break;
            case 'shiseido-ss-product-cushion':
                $url = 'https://www.shiseido.com.my/shiseido-synchro-skin-self-refreshing-cushion-compact-1011575140SHI.html?dwvar_1011575140SHI_color=C1011575440&cgid=M1_Face';
                break;
            case 'shiseido-ss-product-loose-powder':
                $url = 'https://www.shiseido.com.my/shiseido-synchro-skin-invisible-silk-loose-powder-1011579810SHI.html?dwvar_1011579810SHI_color=C1011579710&cgid=M2_Face_Powder';
                break;
            case 'shiseido-ss-product-pressed-powder':
                $url = 'https://www.shiseido.com.my/shiseido-synchro-skin-invisible-silk-pressed-powder-1021612910.html?cgid=M2_Face_Powder';
                break;
            case 'shiseido-ss-product-face-brush':
                $url = 'https://www.shiseido.com.my/shiseido-hanatsubaki-hake-face-brush-1011613610.html?cgid=M2_ToolsAccessories_Brushes';
                break;
            case 'shiseido-ss-product-concealer-brush':
                $url = 'https://www.shiseido.com.my/shiseido-tsutsu-fude-concealer-brush-1011613510.html?cgid=M1_ToolsAccessories';
                break;
        }

        return redirect($url);
    }

    public function generateUniqueCode()
    {
        $allowedChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        while (true) {
            $code = '';

            for ($i = 0; $i < 5; $i++) {
                $code .= $allowedChars[mt_rand(0, strlen($allowedChars) - 1)];
            }

            $existed = DB::table('registrations')->where('unique_code', $code)->count();

            if (!$existed) {
                break;
            }
        }

         return $code;
    }

    public static function makeBookingVoucher($code, $timeslot) // image method. $code
    {
        $im = imagecreatefromjpeg( public_path('3.jpg') );
        $booking  = Makeup_booking::where('image_code', $code)->where('time_slot_id', $timeslot)->with('timeslots')->first();
        $top_name = 'Dear '.$booking->fullname;
        $textbox_1 = imagettfbbox(13, 0, public_path('fonts/READER-BOLD_1.OTF'), $top_name);
        $textbox_2 = imagettfbbox(13, 0, public_path('fonts/READER-BOLD_1.OTF'), $booking->fullname);
        $textbox_3 = imagettfbbox(10, 0, public_path('fonts/Reader-Regular.otf'), $booking->email);
        $textbox_4 = imagettfbbox(10, 0, public_path('fonts/Reader-Regular.otf'), $booking->phone);

        $textbox_5 = imagettfbbox(10, 0, public_path('fonts/Reader-Regular.otf'), $booking->timeslots->artists->name);
        $textbox_6 = imagettfbbox(10, 0, public_path('fonts/Reader-Regular.otf'), $booking->timeslots->date);
        $textbox_7 = imagettfbbox(10, 0, public_path('fonts/Reader-Regular.otf'), $booking->timeslots->time);
        $x_index_1 = (600/2) - (($textbox_1[2]-$textbox_1[0])/2);
        $x_index_2 = (310/2) - (($textbox_2[2]-$textbox_2[0])/2);
        $x_index_3 = (310/2) - (($textbox_3[2]-$textbox_3[0])/2);
        $x_index_4 = (310/2) - (($textbox_4[2]-$textbox_4[0])/2);
        $x_index_5 = 280 + ((310/2) - (($textbox_5[2]-$textbox_5[0])/2));
        $x_index_6 = 280 + ((310/2) - (($textbox_6[2]-$textbox_6[0])/2));
        $x_index_7 = 280 + ((310/2) - (($textbox_7[2]-$textbox_7[0])/2));

        imagettftext($im, 13, 0, $x_index_1 , 30, imagecolorallocate($im, 28,27,26), public_path('fonts/Reader-Regular.otf'), 'Dear');
        imagettftext($im, 13, 0, $x_index_1+42 , 30, imagecolorallocate($im, 156,89,56), public_path('fonts/READER-BOLD_1.OTF'), $booking->fullname);
        imagettftext($im, 13, 0, $x_index_2 , 200, imagecolorallocate($im, 28,27,26), public_path('fonts/READER-BOLD_1.OTF'), $booking->fullname);
        imagettftext($im, 10, 0, $x_index_3, 235, imagecolorallocate($im, 28,27,26), public_path('fonts/Reader-Regular.otf'),$booking->email);
        imagettftext($im, 10, 0, $x_index_4, 270, imagecolorallocate($im, 28,27,26), public_path('fonts/Reader-Regular.otf'),$booking->phone);
        imagettftext($im, 10, 0, $x_index_5, 200, imagecolorallocate($im, 224,26,61), public_path('fonts/Reader-Regular.otf'),$booking->timeslots->artists->name);
        imagettftext($im, 10, 0, $x_index_6, 235, imagecolorallocate($im, 224,26,61), public_path('fonts/Reader-Regular.otf'),$booking->timeslots->date);
        imagettftext($im, 10, 0, $x_index_7, 270, imagecolorallocate($im, 224,26,61), public_path('fonts/Reader-Regular.otf'),$booking->timeslots->time);
        return imagejpeg($im, public_path('booking_voucher/') . $code . '.jpg');
    }

    public static function makeVoucher($code) // image method. $code
    {
        $im = imagecreatefromjpeg( public_path('2.jpg') );
        $registration = DB::table('registrations')->where('unique_code', $code)->first();
        $top_name = 'Dear '.$registration->fullname;
        $textbox_1 = imagettfbbox(13, 0, public_path('fonts/READER-BOLD_1.OTF'), $top_name);
        $textbox_2 = imagettfbbox(13, 0, public_path('fonts/READER-BOLD_1.OTF'), $registration->fullname);
        $textbox_3 = imagettfbbox(10, 0, public_path('fonts/Reader-Regular.otf'), $registration->email);
        $textbox_4 = imagettfbbox(10, 0, public_path('fonts/Reader-Regular.otf'), $registration->phone);
        $textbox_5 = imagettfbbox(10, 0, public_path('fonts/Reader-Regular.otf'), $registration->redeem_location);
        $x_index_1 = (600/2) - (($textbox_1[2]-$textbox_1[0])/2);
        $x_index_2 = (340/2) - (($textbox_2[2]-$textbox_2[0])/2);
        $x_index_3 = (340/2) - (($textbox_3[2]-$textbox_3[0])/2);
        $x_index_4 = (340/2) - (($textbox_4[2]-$textbox_4[0])/2);
        $x_index_5 = (340/2) - (($textbox_5[2]-$textbox_5[0])/2);
        imagettftext($im, 13, 0, $x_index_1 , 30, imagecolorallocate($im, 28,27,26), public_path('fonts/Reader-Regular.otf'), 'Dear');
        imagettftext($im, 13, 0, $x_index_1+42 , 30, imagecolorallocate($im, 156,89,56), public_path('fonts/READER-BOLD_1.OTF'), $registration->fullname);
        imagettftext($im, 13, 0, $x_index_2 , 200, imagecolorallocate($im, 28,27,26), public_path('fonts/READER-BOLD_1.OTF'), $registration->fullname);
        imagettftext($im, 10, 0, $x_index_3, 225, imagecolorallocate($im, 28,27,26), public_path('fonts/Reader-Regular.otf'),$registration->email);
        imagettftext($im, 10, 0, $x_index_4, 250, imagecolorallocate($im, 28,27,26), public_path('fonts/Reader-Regular.otf'),$registration->phone);
        imagettftext($im, 10, 0, $x_index_5, 275, imagecolorallocate($im, 156,89,56), public_path('fonts/Reader-Regular.otf'),$registration->redeem_location);
        return imagejpeg($im, public_path('voucher/') . $code . '.png');
    }

    public function test($code){
        $registration = DB::table('registrations')->where('unique_code', $code)->first();
        return view('email.thank-you-2',['user' => $registration]);
        // return view('email.redeem-ice-cream',['user' => $registration]);
    }
    public function testEmail() // email method, email screenshot method.
    {
        // $email = ['sbfoo@webqlo.com'];
        $timeslot = 1;
        HomeController::makeBookingVoucher($timeslot);
        $booking = DB::table('makeup_bookings')->where('time_slot_id', $timeslot)->first();
        // Mail::to( $email )->send( new ThankYouForRegistering($code) );
        return view('email.thank-you-booking',['booking'=>$booking]);
         // return view('thank-you', ['registration' => $registration]);
        // return view('redeem', ['is_valid' => 0]);
    }

    public function viewPolicy(){
        return view('privacy-policy');
    }

    public function viewTnc(){
        return view('terms-and-conditions');
    }

    public function viewMakeupTnc(){
        return view('makeup-terms-and-conditions');
    }
}