<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Makeup_booking extends Model
{
   	public function timeslots(){
   		return $this->belongsTo('App\Timeslot','time_slot_id', 'id');
   	}
}
