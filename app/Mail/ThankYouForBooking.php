<?php

namespace App\Mail;

use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ThankYouForBooking extends Mailable
{
    use Queueable, SerializesModels;
    public $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->booking = DB::table('makeup_bookings')->where('image_code', $code)->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['Shiseido' => 'no-reply@shiseido.com.my'])
            ->subject('#INSYNCALLWAYS Makeover Slot')
            ->view('email.thank-you-booking');
    }
}
