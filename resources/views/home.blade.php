@extends('layouts.app')
@section('content')
<div class="container-fluid utm-bg">
    <a class="floating-logo-btn" href="{{ Carbon\Carbon::parse( env('END_TIME') )->isFuture() ? '#road_show_form' : '#end_makeup' }}">
        <img class="animated infinite pulse" src="{{ asset('images/floating-button-makeover.png') }}" alt="floating-button">
    </a>
    <div id="top-section">
        @include('header')
        <div class='container-fluid'>
            <div class='row'>
                <div class='col-xs-12 hidden-xs p-0'>
                    <img class='img-responsive' src="{{ asset('images/banner.jpg') }}">
                </div>
                <div class='col-xs-12 hidden-lg hidden-md hidden-sm'>
                    <img class='img-responsive' src="{{ asset('images/banner_mob_1.png') }}">
                </div>
                <div class='col-xs-12 hidden-lg hidden-md hidden-sm mt-15'>
                    <img class='img-responsive' src="{{ asset('images/banner_mob_2.png') }}">
                </div>
            </div>
        </div>
        <div class='container p-0 d-flex'>
            <div class="col-xs-12 text-center top-description m-auto">
                <p class="pb-25">With <strong>ActiveForce<sup>TM</sup> Technology</strong>, self-refreshes nonstop. Synchronizes with skin. Actively helps resist heat, oil, humidity, and motion so you maintain a just-applied look and flawless finish that lasts 24 hours.</p>
                <p>Weightless comfort. Breathable. <br>Blendable. Buildable</p>
            </div>
        </div>
    </div>
    <div id="bottom-section">
        <div class="container mb-50">
            @include('road-show-details')
        </div>
        <div class="container road-show-section mb-100">
            @if( \Carbon\Carbon::parse( env('END_TIME') )->isFuture() )
                @if( !$artist_name_array )
                    <div id="end_makeup" class="text-center mb-100">
                        <p class="color-text">
                            The Makeover slot has been fully booked.
                        </p>
                        <p>
                            Thank you for your participation. Please stay tuned to our upcoming events.
                        </p>
                    </div>
                @else
                    @include('road-show-form')
                @endif
            @else
                <div id="end_makeup" class="text-center mb-100">
                    <p class="color-text">
                        The book your makeover event has ended on {{ Carbon\Carbon::parse( env('END_TIME') )->format('j F Y, ga') }}.
                    </p>
                    <p>
                        Thank you for your participation. Please stay tuned to our upcoming events.
                    </p>
                </div>
            @endif
        </div>
        <div class="container form-section">
            @if( \Carbon\Carbon::parse( env('END_TIME') )->isFuture() )
                @include('form')
            @else
                @include('end')
            @endif
        </div>
        <div class="slider-section">
            <div id="product-sec"> 
                <div class='container slider-container'>
                    <h2 class="color-text">Synchro Skin Self-Refreshing Collection</h2>
                    <div class='row'>
                        <div class="col-xs-12 col-md-12 col-centered">
                            <div class='owl-carousel' id='product-carousel'>   
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img product1" src="{{ asset('images/foundation.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content">
                                            <div class="carousel-title color-text">
                                                Synchro Skin Self-Refreshing Foundation
                                            </div>
                                            <div class="carousel-text">
                                                Self-Refreshes non-stop for a 24-hour just-applied look and flawless finish.
                                            </div>
                                            <div class="carousel-btn">
                                                <a class="learn-more-btn" href="{{ route('redirect', ['target' => 'shiseido-ss-product-foundation']) }}" target="_blank">Learn More</a>
                                            </div>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img" src="{{ asset('images/concealer.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content active">
                                            <div class="carousel-title color-text">
                                                Synchro Skin Self-Refreshing Concealer
                                            </div>
                                            <div class="carousel-text">
                                                Perfecting multi-tasker with 24-hour wear 
                                            </div>
                                            <div class="carousel-btn">
                                                <a class="learn-more-btn" href="{{ route('redirect', ['target' => 'shiseido-ss-product-concealer']) }}" target="_blank">Learn More</a>
                                            </div>
                                        </div>            
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img" src="{{ asset('images/cushion.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content active">
                                            <div class="carousel-title color-text">
                                                Synchro Skin Self-Refreshing Cushion Compact
                                            </div>
                                            <div class="carousel-text">
                                                Self-Refreshes non-stop for a 16-hour just-applied look and flawless finish 
                                            </div>
                                            <div class="carousel-btn">
                                                <a class="learn-more-btn" href="{{ route('redirect', ['target' => 'shiseido-ss-product-cushion']) }}" target="_blank">Learn More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img" src="{{ asset('images/loose-powder.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content active">
                                            <div class="carousel-title color-text">
                                                Synchro Skin Invisible Silk Loose Powder
                                            </div>
                                            <div class="carousel-text">
                                                An air-light, ultra-fine powder that synchronizes with your skin and enhances the finish and wear of your makeup.
                                            </div>
                                            <div class="carousel-btn">
                                                <a class="learn-more-btn" href="{{ route('redirect', ['target' => 'shiseido-ss-product-loose-powder']) }}" target="_blank">Learn More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img" src="{{ asset('images/pressed-powder.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content active">
                                            <div class="carousel-title color-text">
                                                Synchro Skin Invisible Silk Pressed Powder
                                            </div>
                                            <div class="carousel-text">
                                                A silky-smooth pressed powder that synchronizes with your skin for a polished, smooth and shine-free makeup finish.
                                            </div>
                                            <div class="carousel-btn">
                                                <a class="learn-more-btn" href="{{ route('redirect', ['target' => 'shiseido-ss-product-pressed-powder']) }}" target="_blank">Learn More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img" src="{{ asset('images/face-brush.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content active">
                                            <div class="carousel-title color-text">
                                                Hanatsubaki Hake Polishing Face Brush
                                            </div>
                                            <div class="carousel-text">
                                                Four-petal face brush that is uniquely designed to contour the face and buff skin to a polished finish.
                                            </div>
                                            <div class="carousel-btn">
                                                <a class="learn-more-btn" href="{{ route('redirect', ['target' => 'shiseido-ss-product-face-brush']) }}" target="_blank">Learn More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img" src="{{ asset('images/concealer-brush.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content active">
                                            <div class="carousel-title color-text">
                                                Tsutsu Fude Concealer Brush
                                            </div>
                                            <div class="carousel-text">
                                               This cylindrical shaped brush is specially designed for applying, patting and blending liquid or cream concealer over imperfections.
                                            </div>
                                            <div class="carousel-btn">
                                                <a class="learn-more-btn" href="{{ route('redirect', ['target' => 'shiseido-ss-product-concealer-brush']) }}" target="_blank">Learn More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div> 
    </div>
    @include('footer')
</div>
@endsection
@push('modal')
<div class="modal fade" id="privacy_policy" role="dialog" tabindex="-1" role="dialog" aria-labelledby="privacy_policy">
    <div class="modal-dialog modal-lg">               
        <div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title" style="text-align: center; font-weight:bold;">Privacy Policy</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div>
                        <p>Shiseido Malaysia Sdn. Bhd. (<strong>“Shiseido”</strong> or <strong>“We”</strong>) is committed to protecting
                            your privacy and ensuring that your Personal Data is protected. For the purposes of this Privacy Policy,
                            "Personal Data" means any personally identifiable data, whether true or not, about an individual who can be
                            identified from that data.</p>
                    </div>
                    <div>
                        <div>
                            <h3>1. APPLICATION</h3>
                            <p>This Privacy Policy explains the types of Personal Data we collect and how we use, disclose, transfer,
                                process and protect that information.</p>
                            <p>We collect Personal Data through, but not limited to, the following means:</p>
                            <div>
                                <ol type="a">
                                    <li>When you shop on or browse <a href="https://www.shiseido.com.my" rel="shiseido website">[https://www.shiseido.com.my]</a> (the <strong>“Website”</strong>);
                                    </li>
                                    <li>When you shop in-store at our physical stores;</li>
                                    <li>When you connect with us through social media or attend our marketing events; and</li>
                                    <li>When you agree and consent to be a member of the Shiseido Membership, whether through
                                        physical or electronic means.
                                    </li>
                                </ol>
                            </div>
                            <p>We may update this Privacy Policy from time to time by posting updated versions on the Website, and/or by
                                sending an e-mail to you. Your continued membership, access to and/or use of the Website will be taken
                                to be your agreement to, and acceptance of, all changes made in each updated version.<br>Please check
                                back regularly for updated information on how we handle your Personal Data.</p>
                        </div>
                        <div>
                            <h3>2. CONSENT</h3>
                            <p>We do not collect, use or disclose your Personal Data without your consent (except where permitted and
                                authorised by law). By providing your Personal Data to us, you hereby consent to us collecting, using,
                                disclosing, transferring, and processing your Personal Data for the purposes set out in Section 3 of
                                this Privacy Policy.</p>
                            <p>The types of Personal Data we collect include, but are not limited to, your: (a) first name and family
                                name; (b) home address; (c) date of birth; (d) email address; and, only if appropriate, your (e) user
                                name and password; (f) billing and delivery address; (g) personal identification number; and (h) other
                                information as may be reasonably required for us to provide you with the Services as defined in Section
                                3 below.</p>
                        </div>
                        <div>
                            <h3>3. PURPOSE</h3>
                            <p>We collect, use, disclose, transfer and process your Personal Data for the purpose of providing services.
                                These services include, but are not limited to:</p>
                            <div>
                                <ol>
                                    <li>providing you with information on products and campaigns from us, Shiseido Group and our
                                        third party business partners via email, SMS, and post (where we have your express consent);
                                    </li>
                                    <li>allowing you to purchase products and services offered for sale via the Website;</li>
                                    <li>facilitating your transactions with us;</li>
                                    <li>sending you product samples and/or products;</li>
                                    <li>keeping you informed of updates, changes, and developments relating to us and our Services;
                                    </li>
                                    <li>notifying you about important changes to this Privacy Policy, and to our other policies or
                                        services;
                                    </li>
                                    <li>providing you with personalized consultations;</li>
                                    <li>responding to queries or feedback from you;</li>
                                    <li>maintaining and operating the Website;</li>
                                    <li>managing our administrative and business operations;</li>
                                    <li>engaging third party business partners and data processors to perform certain aspects of the
                                        Services;
                                    </li>
                                    <li>performing customer profiling, market analysis, and research to improve our product and
                                        service offerings to you;
                                    </li>
                                    <li>preventing, detecting and investigating crime and analysing and managing commercial risks; <br>and
                                    </li>
                                    <li>other purposes which are reasonably related to the above.</li>
                                </ol>
                            </div>
                            <p>(collectively, the <strong>"Services"</strong>)</p>
                        </div>
                        <div>
                            <h3>4. WITHDRAWAL OF CONSENT, ACCESS & CORRECTION</h3>
                            <p>If you wish to withdraw your consent to receive information on new products and campaigns, or any other
                                Services, you may do so by:</p>
                            <div>
                                <ol>
                                    <li>unsubscribing from our Website;</li>
                                    <li>clicking the “Unsubscribe" link in the email(s) we send to you;</li>
                                    <li>contacting our Data Protection Officer at the email address below; or</li>
                                    <li>writing to us at the address below.</li>
                                </ol>
                            </div>
                            <p>Please note that if you choose not to provide us with certain Personal Data, or to withdraw your consent
                                to our use, disclosure, transfer and/or processing of your Personal Data, we may not be able to provide
                                you with some or all of the Services.</p>
                            <p>We will ensure that the Personal Data in our possession is accurate and complete to the best of our
                                knowledge.</p>
                            <p>You have a right to request for access and correction of your Personal Data. If you would like assistance
                                in accessing and/or correcting your Personal Data, please contact our Data Protection Officer at the
                                email address below. We will get back to you within 21 days.</p>
                        </div>
                        <div>
                            <h3>5. CHILDREN</h3>
                            <p>This Website is directed toward and designed for use by persons aged 16 or older. We do not intend to
                                collect Personal Data from children under 16 years of age, except on some sites specifically directed to
                                children.</p>
                            <p>We protect the Personal Data of children along with the necessary parental consent in the same manner as
                                it protects the Personal Data of adults.</p>
                        </div>
                        <div>
                            <h3>6. THIRD PARTY DISCLOSURE & TRANSFER</h3>
                            <p>We do not disclose or transfer your Personal Data to third parties unless we have clearly asked for and
                                obtained your consent to do so (except where permitted and authorised by law).</p>
                            <p>The Personal Data which you provide to us may be stored, processed, transferred between, and accessed
                                from servers located in the United States and other countries which have laws and regulations that may
                                not guarantee the same level of protection of Personal Data as Malaysia. However, we will take
                                reasonable steps to ensure that your Personal Data is handled in accordance with this Privacy Policy,
                                regardless of where your Personal Data is stored or accessed from.</p>
                            <div>
                                <dl>
                                    <dt>6.1 Disclosure to affiliated companies in the Shiseido Group</dt>
                                    <dd>The Shiseido Group comprises a number of affiliated companies and legal entities located both
                                        within and outside Malaysia. We may disclose, where appropriate and to the extent necessary,
                                        your Personal Data to such affiliated companies and legal entities for the purposes of corporate
                                        reporting, market research and analysis, customer relationship management and other related
                                        legal and business purposes. Please note that we provide our affiliated companies and legal
                                        entities with only the Personal Data they need for such business and legal purposes, and we
                                        require that they protect such Personal Data in accordance with the applicable laws and
                                        regulations and this Privacy Policy, and not use it for any other purpose.
                                    </dd>
                                    <dt>6.2 Disclosure to third party business partners</dt>
                                    <dd>We rely on third party business partners to perform a variety of services on our behalf. In so
                                        doing, Shiseido may let service providers, located both within and outside Malaysia, use your
                                        Personal Data for the marketing and promotion of products, services or events that may be of
                                        interest to you, for market research and analysis, for customer relationship management, and for
                                        the fulfilment of your orders for products and services purchased via the Website. Please note
                                        that we provide our third party business partners with only the Personal Data they need to
                                        perform their services and we require that they protect such Personal Data in accordance with
                                        the applicable laws and regulations and this Privacy Policy, and not use it for any other
                                        purpose.
                                    </dd>
                                    <dt>6.3 Disclosure to third party data processors</dt>
                                    <dd>We may use third party service providers, located both within and outside Malaysia, to help us
                                        maintain and operate the Website, or for other reasons related to the operation of the Website
                                        and Shiseido’s business, and they may receive your Personal Data for these purposes. We only
                                        provide them the Personal Data they need to provide these services on our behalf. We require
                                        these companies to protect the Personal Data in accordance with the applicable laws and
                                        regulations and this Privacy Policy, and to not use the information for any other purpose.
                                    </dd>
                                    <dt>6.4 Other disclosure</dt>
                                    <dd>We may use and disclose your Personal Data to perform your instructions and, as relevant, (a)
                                        comply with legislative and regulatory requirements; (b) protect or defend rights or properties
                                        of customers and employees of Shiseido; and/or (c) take emergency measures for the purpose of
                                        securing the safety of customers, Shiseido, or the general public. We may also disclose and
                                        transfer our personal data to other service providers and/or third parties (which may be located
                                        both within and outside Malaysia) in the context of a merger, acquisition or any other corporate
                                        exercise involving Shiseido.
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <div>
                            <h3>7. SECURITY & PROTECTION</h3>
                            <p>We maintain strict procedures, standards, and security arrangements to protect Personal Data in our
                                possession or under our control. Upon receipt of your Personal Data, whether through physical or
                                electronic means of collection, we will make the necessary security arrangements to protect such
                                Personal Data as are reasonable and appropriate in the circumstances. Such arrangements may comprise
                                administrative measures, physical measures, technical measures, or a combination of such measures.</p>
                            <p>When disclosing or transferring your Personal Data over the internet, we take all reasonable care to
                                prevent unauthorised access to your Personal Data. However, no data transmission over the internet can
                                be guaranteed as fully secure and you acknowledge that you submit information over the internet at your
                                own risk.</p>
                        </div>
                        <div>
                            <h3>8. RETENTION OF PERSONAL DATA</h3>
                            <p>We will not retain your Personal Data for any period of time longer than is necessary to serve the
                                purposes set out in this Privacy Policy and any valid business or legal purposes. After this period of
                                time, we will destroy or anonymise any documents containing your Personal Data in a safe and secure
                                manner.</p>
                        </div>
                        <div>
                            <h3>9. GOVERNING LAW</h3>
                            <p>This Privacy Policy is governed by Malaysia law.</p>
                        </div>
                        <div>
                            <h3>10. CONTACT US</h3>
                            <p>If you would like to access or correct any Personal Data which you have provided to us, submit a
                                complaint, or have any queries about your Personal Data, please contact our Data Protection Officer by
                                contacting us at <a
                                        href="mailto:shiseidocamellia@shiseido.com.my">shiseidocamellia@shiseido.com.my</a>.
                                Alternatively, you may write to us at:<br>
                                Shiseido Malaysia Sdn.Bhd.<br>
                                Unit 7-03, Level 7, Menara UAC, No.12, Jalan PJU 7/5, Mutiara Damansara, 47800 Petaling Jaya, Selangor
                                Darul Ehsan,Malaysia<br>
                                Tel: 60-3-7719-1888
                            </p>
                        </div>
                        <div>
                            <p>Date: 20 July 2018 </p>
                        </div>
                    </div>
                </div>
                <!-- /#main -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="consent" role="dialog" tabindex="-1" role="dialog" aria-labelledby="consent">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center; font-weight:bold;">Consent</h4>
            </div>
            <div class="modal-body">
                <p>
                    You hereby agree and consent to Shiseido Malaysia Sdn. Bhd. ("Shiseido") collecting, using, disclosing, transferring, and processing your personal data, in accordance with Shiseido's Privacy Policy (accessible online at <a href="https://www.shiseido.com.my/privacypolicy" target="_blank">https://www.shiseido.com.my/privacypolicy</a>).
                    <br />
                    By providing your personal data to us, you agree and consent to; 
                </p>
                <ol type="a">
                    <li>be included in our databases for our sales and marketing opportunities, including for establishing an account for checkout purposes if applicable; and</li>
                    <li>Shiseido providing, or continuing to provide, you with the Services described in Section 3 of Shiseido's Privacy Policy.</li>
                </ol>
                <p>
                    You specifically agree and consent to the transfer of your personal data to affiliated companies within the Shiseido Group and to third party service providers hired by Shiseido to process such data, both within and outside of Malaysia, in accordance with Shiseido's Privacy Policy.
                    <br />
                    You would also like to receive information on products and campaigns from Shiseido, Shiseido Group and our third party business partners via email, SMS and post. 
                </p>
                <ul>
                    <li>I consent to receiving such information as stated above.</li>
                    <li>I consent to Shiseido's use of my skin-related information and health-related information in accordance with Shiseido's Privacy Policy.</li>
                </ul>
            </div><!-- modal body -->
        </div>
    </div>
</div> 
<div class="modal fade" id="termsNconditions" role="dialog" tabindex="-1" role="dialog" aria-labelledby="terms_and_conditions">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center; font-weight:bold;">Terms & Conditions</h4>
            </div>
            <div class="modal-body">
                <ol>
                    <li>This Campaign is organized by Shiseido Malaysia Sdn Bhd.</li>
                    <li>The exclusive Sample Kit are while stocks last only. One redemption per customer please. Duplicate redemptions will not be entertained.</li>
                    <li>The Campaign will run on Facebook from 1<sup>st</sup> Sep 2019 until 31<sup>st</sup> Oct 2019.</li> 
                    <li>The Campaign is open to residents of Malaysia and permanent residents aged 18 years old and above with valid NRIC. By entering the campaign you warrant that you meet these requirements.</li>
                    <li>The following group of persons shall not be eligible to participate in the campaign: Employees of the Organizer including its affiliated and related companies and their immediate family members (children, parents, brothers and sisters, including spouses); and/or representatives, employees, servants and/or agents of advertising and/or promotion service providers of Organizer including its affiliated and related companies, and their immediate family members (children, parents, brothers and sisters including spouses).</li>
                    <li>The Organizer reserves the right to suspend, modify, terminate or cancel the campaign at any time. These conditions may be amended from time to time by the Organizer without prior notice. All entries received outside the campaign period shall automatically be disqualified.</li>
                    <li>The Organizer reserves the right at its absolute discretion to substitute any of the mystery gift at any time without prior notice. All mystery gifts are given on an “as is” basis.</li>
                    <li>Failure by the Organizer to enforce any of its rights at any time does not constitute a waiver of those rights.</li>
                    <li>This Campaign is in no way sponsored, endorsed or administered by, or associated with Facebook. You are providing your information to Shiseido Malaysia Sdn. Bhd. and not to Facebook. The information you provide will only be used for the fulfillment of this campaign only.</li>
                </ol>
            </div><!-- modal body -->
        </div>
    </div>
</div>
<div class="modal fade" id="makeup_termsNconditions" role="dialog" tabindex="-1" role="dialog" aria-labelledby="makeup_terms_and_conditions">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center; font-weight:bold;">Terms & Conditions</h4>
            </div>
            <div class="modal-body">
                <ol>
                    <li>This #InSyncAllWays Makeover Slot is organized by Shiseido Malaysia Sdn Bhd.</li>
                    <li>Limited to 1 customer per time slot. No duplicate time slots will be entertained.</li>
                    <li>Customer can only reserve 1 slot only.</li> 
                    <li>We Reserve the right, in our discretion to amend the terms & conditions.</li>
                    <li>Late Arrivals (10 mins) will result in cancellation of appointments.</li>
                    <li>The Campaign is open to residents of Malaysia and permanent residents aged 18 years old and above with valid NRIC. By entering the campaign you warrant that you meet these requirements.</li>
                    <li>The following group of persons shall not be eligible to participate in the campaign: Employees of the Organizer including its affiliated and related companies and their immediate family members (children, parents, brothers and sisters, including spouses); and/or representatives, employees, servants and/or agents of advertising and/or promotion service providers of Organizer including its affiliated and related companies, and their immediate family members (children, parents, brothers and sisters including spouses).</li>
                    <li>The Organizer reserves the right to suspend, modify, terminate or cancel the campaign at any time. These conditions may be amended from time to time by the Organizer without prior notice. All entries received outside the campaign period shall automatically be disqualified.</li>
                    <li>The Organizer reserves the right at its absolute discretion to substitute any of the mystery gift at any time without prior notice. All mystery gifts are given on an “as is” basis.</li>
                    <li>Failure by the Organizer to enforce any of its rights at any time does not constitute a waiver of those rights.</li>
                    <li>This Campaign is in no way sponsored, endorsed or administered by, or associated with Facebook. You are providing your information to Shiseido Malaysia Sdn. Bhd. and not to Facebook. The information you provide will only be used for the fulfillment of this campaign only.</li>
                </ol>
            </div><!-- modal body -->
        </div>
    </div>
</div>
@endpush