<div id="registration_form">
    <div class="panel panel-default">
        <div class="panel-heading text-center panel-custom-css">
            <h2>
                Redeem Your <span class="color-text">Synchro Skin Self Refreshing</span> Sample Kit Now. 
            </h2>
        </div>
        <div class="panel-body panel-color-background">
            <form>
            <div class="form-group" id="fullname_required">
                <label class="label-required" for="fullname">Name(as per your NRIC)</label>
                <input id="fullname" class="form-control" type="text" name="fullname" placeholder="eg: John Doe" />
                <span class="help-block label label-danger"></span>
            </div>

            <div class="form-group" id="phone_required">
                <label class="label-required" for="phone">Mobile No.</label>
                <input id="phone" class="form-control" type="text" name="phone" placeholder="eg: 0123456789" />
                <span class="help-block label label-danger"></span>
            </div>

            <div class="form-group" id="email_required">
                <label class="label-required" for="email">Email</label>
                <input id="email" class="form-control" type="text" name="email" placeholder="eg: john@email.com" />
                <span class="help-block label label-danger"></span>
            </div>

            <div class="form-group checkbox_form" id="location_required">
                <label class="label-required" for="location">I would like to receive my sample at</label>
                <select id="location" class="form-control" name="location">
                    <option value="">Please choose one</option>
                    <optgroup label="KUALA LUMPUR / SELANGOR">
                        <option>ISETAN KLCC</option>
                        <option>ISETAN THE GARDENS MALL</option>
                        <option>ISETAN BANDAR UTAMA</option>
                        <option>ISETAN LOT 10</option>
                        <option>AEON MID VALLEY MEGAMALL</option>
                        <option>AEON BANDAR UTAMA</option>
                        <option>AEON METRO PRIMA</option>
                        <option>AEON TAMAN MALURI</option>
                        <option>AEON BUKIT TINGGI</option>
                        <option>PARKSON PAVILION</option>
                        <option>PARKSON BANDAR UTAMA</option>
                        <option>PARKSON OUG</option>
                        <option>PARKSON SUBANG</option>
                        <option>PARKSON SUNWAY PYRAMID</option>
                        <option>PARKSON SEREMBAN PARADE</option>
                        <option>PARKSON KLANG PARADE</option>
                        <option>PARKSON NU SENTRAL</option>
                        <option>PARKSON VELOCITY</option>
                        <option>SOGO KL</option>
                        <option>SOGO I-CITY MALL</option>
                        <option>SUNWAY PYRAMID</option>
                        <option>ROBINSONS KLCC</option>
                    </optgroup>
                    <optgroup label="EAST COST PENINSULAR">
                        <option>PARKSON EAST COAST MALL</option>
                        <option>PACIFIC KOTA BHARU MALL</option>
                        <option>PACIFIC MENTAKAB</option>
                    </optgroup>
                    <optgroup label="SOUTH PENINSULAR">
                        <option>PARKSON MELAKA</option>
                        <option>PARKSON SQUARE ONE</option>
                        <option>PARKSON HOLIDAY PLAZA</option>
                        <option>AEON BANDARAYA MELAKA</option>
                        <option>AEON TEBRAU CITY</option>
                        <option>AEON BUKIT INDAH</option>
                        <option>THE STORE KOMPLEKS LIEN HOE</option>
                        <option>THE STORE MUAR</option>
                        <option>PACIFIC MALL BATU PAHAT</option>
                        <option>PARKSON PARADIGM MALL (JOHOR BAHRU)</option>
                    </optgroup>
                    <optgroup label="CENTER / NORTHEN PENINSULAR">
                        <option>AEON KINTA CITY</option>
                        <option>AEON QUEENSBAY MALL</option>
                        <option>PARKSON IPOH</option>
                        <option>PARKSON GURNEY PLAZA</option>
                        <option>PARKSON SUNWAY CARNIVAL</option>
                        <option>PARKSON AMAN CENTRAL ALOR SETAR</option>
                        <option>PACIFIC PRAI</option>
                        <option>PACIFIC TAIPING MALL</option>
                        <option>SUNSHINE SQUARE</option>
                    </optgroup>
                    <optgroup label="SARAWAK / BRUNEI">
                        <option>PARKSON THE SPRING SHOPPING MALL</option>
                        <option>PARKSON SIBU</option>
                        <option>PARKSON MIRI</option>
                        <option>FARLEY BINTULU</option>
                        <option>HUA HO YAYASAN</option>
                    </optgroup>
                    <optgroup label="SABAH">
                        <option>METRO SURIA KOTA KINABALU</option>
                        <option>PARKSON IMAGO MALL</option>
                    </optgroup>
                </select>
                <span class="help-block label label-danger"></span>
            </div>

            <div class="form-group checkbox_form">
                <label for="receive_update">I would like to receive Shiseido updates via</label>
                <select id="receive_update" class="form-control" name="receive_update">
                    <option value="">Please choose one</option>
                    <option>SMS</option>
                    <option>E-Newsletter</option>
                    <option>SMS & E-Newsletter</option>
                </select>
            </div>

            <div class="form-group disclaimer-text">
                By clicking “Submit”, I agree to receive occasional email updates on Shiseido related promotions and activities.
            </div>

            <div class="form-group tnc-text" id="tnc_required">
                <div class="checkbox">
                    <label class="label-required">
                        <input id="tnc" type="checkbox" name="tnc" onchange="handleChange(this);" /> I have read and agreed to the <a data-toggle="modal" data-target="#consent">Consent</a>, <a class='agree-text' data-toggle="modal" data-target="#privacy_policy">Privacy Policy</a> and <a data-toggle="modal" data-target="#termsNconditions">Terms and Conditions</a></label>
                </div>
                <span class="help-block label label-danger"></span>
            </div>
            <div class='col-xs-12 submit-section'>
                <button id="submit-form" class="btn btn-default btn-block custom-submit-btn btn-lg" type="submit" onclick="this.disabled=true;submitForm(event);">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
@push('js')
    <script>
        $(function () {
            $('input').change(function () {
                $(this).parent('.has-error').removeClass('has-error').find('.help-block').css('display','none');
            });
            $('select').on('change', function () {
                $(this).parent('.has-error').removeClass('has-error').find('.help-block').css('display','none');
            });
        });
        function handleChange(checkbox){
            if(checkbox.checked == true){
                var get_name = $(checkbox).attr('name');
                $('#'+get_name+'_required').removeClass('has-error').find('.help-block').css('display','none');
            }
        }
        function dbUrl(){
            var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
            if (iOS) {
                return "{{ env('APP_URL') }}/submit-form";
            }else{
                return "/submit-form";
            }
        }
        function submitForm(e){
            var fullname = $('#fullname').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var location = $('#location').val();
            var receive_update = $('#receive_update').val();
            var tnc = $('#tnc').prop('checked');
            e.preventDefault();
            $("span.help-block.label-danger").css('display','none');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: dbUrl(),
                data:{
                    fullname: fullname,
                    phone: phone,
                    email: email,
                    location: location,
                    receive_update: receive_update,
                    tnc: tnc
                },
                success: function(data){
                    $.each(data, function(k, v){
                        if( k == 'success' ){
                            if ( {{ env('LIVE') }} == 1){
                                gtag('event','submit',{
                                    event_category: 'registration'
                                });
                            }
                            window.location.replace("{{ env('APP_URL') }}/thank-you/"+v);
                        }else{
                            $("#submit-form").removeAttr('disabled');
                            $("#"+k+"_required").addClass('has-error');
                            $("#"+k+"_required span.help-block.label-danger").css('display','inline-block');
                            $("#"+k+"_required span.help-block.label-danger").html(v);
                        }
                    });
                }
            });
        }
    </script>
@endpush