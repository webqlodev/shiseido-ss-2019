@extends('layouts.app')
@section('content')
<div class="thank-you-container">
    <a href="{{ env('APP_URL') }}">
        <div id="booking-thank-you" class="thank-you-image">
            <div class="thank-you-image-inner">
                <img src="{{ asset('images/thank-you-page.png')}}" alt="still-valid">
                <div class="thank-you-text">
                    <p>
                    <span class="custom-spacing">Thank you for your details!</span><br>
                    Your details have been sent to your email and we will contact you for your confirmation soon.
                    </p>
                </div>
            </div>
        </div>
    </a>
</div>
@endsection
