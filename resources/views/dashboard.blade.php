@extends('layouts.admin')
@section('content')
@if ( Auth::user()->id <= 2 )
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Total Registration by Date</strong></div>
            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-total-count') }}" target="_blank">Export XLSX</a>
                </div>
                <table id="total_registration_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Total Registration</th>
                            <th>Total Redeemed</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Total Registration by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        @if ( Auth::user()->username == 'webqlo' )
            <blockquote>
                <strong>End Time: </strong>{{ Carbon\Carbon::parse( env('END_TIME') )->format('j F Y, ga') }}
            </blockquote>

            <div class="panel panel-default">
                <div class="panel-heading"><strong>Total Link Clicks</strong></div>

                <div class="panel-body">
                    <table class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Facebook</th>
                                <th>Facebook(email)</th>
                                <th>YouTube</th>
                                <th>YouTube(email)</th>
                                <th>Home page</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ DB::table('clicks')->where('target', 'facebook')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'facebook-email')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'youtube')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'youtube-email')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'homelink')->count() }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Total Product Link Clicks</strong></div>

                <div class="panel-body">
                    <table class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Synchro Skin Self-Refreshing Foundation</th>
                                <th>Synchro Skin Self-Refreshing Cushion Compact</th>
                                <th>Synchro Skin Self-Refreshing Concealer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ DB::table('clicks')->where('target', 'shiseido-ss-product-foundation')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'shiseido-ss-product-cushion')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'shiseido-ss-product-concealer')->count() }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Synchro Skin Invisible Silk Loose Powder</th>
                                <th>Synchro Skin Invisible Silk Pressed Powder</th>
                                <th>Hanatsubaki Hake Face Brush</th>
                                <th>Tsutsu Fude Concealer Brush</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ DB::table('clicks')->where('target', 'shiseido-ss-product-loose-powder')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'shiseido-ss-product-pressed-powder')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'shiseido-ss-product-face-brush')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'shiseido-ss-product-concealer-brush')->count() }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>       
        @endif
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Total Redeemed</strong></div>

            <div class="panel-body">
                <table class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Total Registration</th>
                            <th>Total Redeemed</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ DB::table('registrations')->count() }}</td>
                            <td>{{ DB::table('registrations')->where('redeemed', 1)->count() }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Makeover Booking List</strong></div>

            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-booking-list') }}" target="_blank">Export XLSX</a>
                </div>

                <table id="booking_list_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Phone Number</th>
                            <th>Email Address</th>
                            <th>Makeup Artist</th>
                            <th>Date</th>
                            <th>Time</th>
                            <!-- <th>Redeemed</th> -->
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Registration List by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@if ( Auth::user()->id <= 2 )
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Registration List</strong></div>

            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-registration-list') }}" target="_blank">Export XLSX</a>
                </div>

                <table id="registration_list_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Register Date</th>
                            <th>Email Address</th>
                            <th>Phone Number</th>
                            <th>Redeem Location</th>
                            <th>Redeemed</th>
                            @if ( Auth::user()->username == 'webqlo' )
                            <th>Redeem Code</th>
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Registration List by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="cancelModalLabel">Release Booking</h4>
      </div>
      <div class="modal-body">
        <p>
        Are you sure you want to release this booking?
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="cancel_booking" class="btn btn-primary">Confirm</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script>
$(function () {
    $('#total_registration_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('datatables-total-registration') }}',
        columns: [
            {data: 'date', name: 'date', orderable: false},
            {data: 'total', name: 'total', orderable: false},
            {data: 'total_redeem', name: 'total_redeem', orderable: false}
        ]
    });
    $('#registration_list_table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('datatables-registration-list') }}',
        order: [],
        columns: [
            {
                data: 'fullname'
            },
            {
                data: 'time'
            },
            {
                data: 'email'
            },
            {
                data: 'phone'
            },
            {
                data: 'location'
            },
            {
                render: function(data, type, row, meta) {
                    if( row.time_redeem != 'Yet to redeem.' ){
                        return '<span class="label label-success">'+row.time_redeem+'</span>';    
                    }
                    return '<span class="label label-danger">'+row.time_redeem+'</span>';
                }
            }
            @if ( Auth::user()->username == 'webqlo' )
            ,
            {
                data: 'code'
            },
            {
                render: function(data, type, row, meta) {
                    return '<a class="btn btn-default btn-sm" href="/admin/resend-email/' + row.code + '" target="_blank">Resend Email</a>';
                }
            }
            @endif
        ]
    });
    $('#booking_list_table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('datatables-booking-list') }}',
        order: [],
        columns: [
            {
                data: 'fullname'
            },
            {
                render: function(data, type, row, meta) {
                    if( !row.cancelled_phone ){
                        return row.phone;
                    }else{
                        return row.cancelled_phone;
                    }
                }
            },
            {
                render: function(data, type, row, meta) {
                    if( !row.cancelled_email ){
                        return row.email;
                    }else{
                        return row.cancelled_email;
                    }
                }            
            },
            {
                data: 'artist'
            },
            {
                data: 'date'
            },
            {
                data: 'time'
            },
            // {
            //     render: function(data, type, row, meta) {
            //         if( row.time_redeem != 'Yet to redeem.' ){
            //             return '<span class="label label-success">'+row.updated_at+'</span>';    
            //         }
            //         return '<span class="label label-danger">'+row.updated_at+'</span>';
            //     }
            // },
            {
                render: function(data, type, row, meta) {
                    if( row.cancelled_time_slot != 0 ){
                        return '<span class="label label-danger">This booking is cancelled</span>';
                    }else{
                       return '<button data-id="' + row.id + '" type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#cancelModal">Cancel</button>';
                    }
                    
                }
            }
        ]
    });
    $('#booking_list_table').on( 'draw.dt', function () {
        $('#cancelModal').on('shown.bs.modal', function (e) {
            var get_id = $(e.relatedTarget).attr('data-id');
            $('#cancel_booking').on('click', function(){
                window.location.href = "{{ env('APP_URL') }}/admin/cancel-booking/" + get_id;
            });
        });
    });
});
</script>
@endpush
