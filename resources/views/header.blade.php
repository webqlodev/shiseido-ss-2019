 <nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/" rel="home page"><img class="nav-logo" src="{{ asset('images/shiseido-logo.png') }}" height="auto" width="auto"></a>
        </div>
        <div class="navbar-container container">
            <ul class="nav navbar-nav">
              {{--<li><a href="#app"><span>Home</span></a></li>--}}
              <li><a href="#product-sec"><span>Products</span></a></li>
              <li><a href="{{ Carbon\Carbon::parse( env('END_TIME') )->isFuture() ? '#registration_form' : '#end' }}"><span>Free Sample</span></a></li>
              <li><a href="{{ Carbon\Carbon::parse( env('END_TIME') )->isFuture() ? '#road_show_form' : '#end_makeup' }}"><span>Book Your Slot</span></a></li>
            </ul>
        </div>
    </div>
</nav>