@extends('layouts.app')
@section('content')
	<div class="container mb-100 mt-15">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h1 class="mb-50">Terms & Conditions</h1>
			</div>
			<div class="col-xs-12">
				<ol>
                    <li>This Campaign is organized by Shiseido Malaysia Sdn Bhd.</li>
                    <li>The exclusive Sample Kit are while stocks last only. One redemption per customer please. Duplicate redemptions will not be entertained.</li>
                    <li>The Campaign will run on Facebook from 1<sup>st</sup> Sep 2019 until 31<sup>st</sup> Sep 2019.</li> 
                    <li>The Campaign is open to residents of Malaysia and permanent residents aged 18 years old and above with valid NRIC. By entering the campaign you warrant that you meet these requirements.</li>
                    <li>The following group of persons shall not be eligible to participate in the campaign: Employees of the Organizer including its affiliated and related companies and their immediate family members (children, parents, brothers and sisters, including spouses); and/or representatives, employees, servants and/or agents of advertising and/or promotion service providers of Organizer including its affiliated and related companies, and their immediate family members (children, parents, brothers and sisters including spouses).</li>
                    <li>The Organizer reserves the right to suspend, modify, terminate or cancel the campaign at any time. These conditions may be amended from time to time by the Organizer without prior notice. All entries received outside the campaign period shall automatically be disqualified.</li>
                    <li>The Organizer reserves the right at its absolute discretion to substitute any of the mystery gift at any time without prior notice. All mystery gifts are given on an “as is” basis.</li>
                    <li>Failure by the Organizer to enforce any of its rights at any time does not constitute a waiver of those rights.</li>
                    <li>This Campaign is in no way sponsored, endorsed or administered by, or associated with Facebook. You are providing your information to Shiseido Malaysia Sdn. Bhd. and not to Facebook. The information you provide will only be used for the fulfillment of this campaign only.</li>
                </ol>
			</div>
		</div>
	</div>
@endsection