@extends('layouts.email')
@section('content')
<!--[if (gte mso 9)|(IE)]>
<table border="0" align="center" cellspacing="0" width="600" style="width:600px; max-width: 600px !important;">
  <tr>
    <td>
<![endif]-->
<table border="0" cellspacing="0" align="center" bgcolor="#fff0e8" style="background-color:#fff0e8;width:100%; max-width: 600px !important;">
    <tr>
        <td style="text-align: center;">
             <img src="{{ asset('images/email/banner.jpg') }}" alt="Logo" style="width:100%;max-width: 100%;"/>
        </td>
    </tr>
    <tr align="center">
        <td style="text-align: center; padding: 0 15px !important; position: relative;">
            <a href="{{ env('APP_URL') }}/redeem-sample-kit/{{ $registration->id }}/{{ $registration->unique_code }}" target="_blank">
              <img src="{{ asset('voucher/'.$registration->unique_code.'.png') }}" alt="table" style="width:100%;max-width: 100%;"/>
            </a>
        </td>
    </tr>
    <tr align="center">
        <td style="text-align: center; padding: 0 15px !important; position: relative;">
          <img src="{{ asset('images/email/text3.jpg') }}" alt="text-image" style="width:100%;max-width: 100%;"/>    
        </td>
    </tr>
    <tr align="center"><td><img src="{{ asset('images/email/line.png') }}" alt="line" /></td></tr>
    <tr>
        <td style="padding: 0 15px;">
           <table border="0" cellpadding="0">
              <tr>
                <td align="center" width="600">
                  <p style="font-size: 0.7em; color: #1c1b1a;">Follow Shiseido on Facebook</p>
                </td>
              </tr>
              <tr>
                  <td align="center" width="600">
                      <a href="{{ route('redirect', ['target' => 'facebook-email']) }}" target="_blank"><img src="{{ asset('images/email/fb-icon.png') }}" alt="button" /></a>
                       &nbsp;&nbsp;&nbsp;&nbsp;
                      <a href="{{ route('redirect', ['target' => 'youtube-email']) }}" target="_blank"><img src="{{ asset('images/email/youtube-icon.png') }}" alt="button"/></a>
                  </td>
              </tr>
              <tr>
                  <td align="center" width="600">
                      <p style="font-size: 0.7em;color: #1c1b1a;"><a href="{{ route('terms-and-conditions') }}" target="_blank" style="color: #1c1b1a;">Terms & Conditions</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('privacy-policy') }}" target="_blank" style="color: #1c1b1a;">Privacy Policy</a>
                        <br/>Copyright &#9400;2019 Shiseido Co. Ltd. All rights reserved.</p>
                  </td>
              </tr>
           </table> 
        </td>
    </tr>
</table>
<!--[if (gte mso 9)|(IE)]>
    </td>
  </tr>
</table>
<![endif]-->
@endsection