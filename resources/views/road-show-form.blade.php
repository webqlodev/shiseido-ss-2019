<div class="loading-container"><div class="loader"></div></div>
<div id="road_show_form">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h2>
                Book Your <span class="color-text">#InSyncAllWays</span> Makeover Slot
            </h2>
        </div>
        <div class="panel-body">
            <form id="booking_makeup_form">
            <div class="form-group" id="roadshow_fullname_required">
                <label class="label-required" for="roadshow_fullname">Name(as per your NRIC)</label>
                <input id="roadshow_fullname" class="form-control" type="text" name="roadshow_fullname" placeholder="eg: John Doe" />
                <span class="help-block label label-danger"></span>
            </div>

            <div class="form-group" id="roadshow_phone_required">
                <label class="label-required" for="roadshow_phone">Mobile No.</label>
                <input id="roadshow_phone" class="form-control" type="text" name="roadshow_phone" placeholder="eg: 0123456789" />
                <span class="help-block label label-danger"></span>
            </div>

            <div class="form-group" id="roadshow_email_required">
                <label class="label-required" for="roadshow_email">Email</label>
                <input id="roadshow_email" class="form-control" type="text" name="roadshow_email" placeholder="eg: john@email.com" />
                <span class="help-block label label-danger"></span>
            </div>

            <div class="form-group checkbox_form" id="roadshow_artist_required">
                <label class="label-required" for="roadshow_artist">Makeup Artist</label>
                <select id="roadshow_artist" class="form-control" name="roadshow_artist">
                        <option value="">Please choose one</option>
                        @foreach( $artists_name as $id => $artist_name )
                            @if ( in_array($artist_name, $artist_name_array) )
                                <option value="{{ $id }}">{{ $artist_name }}</option>
                            @else
                                <option value="{{ $id }}" disabled>{{ $artist_name }}</option>
                            @endif
                        @endforeach
                </select>
                <span class="help-block label label-danger"></span>
            </div>

            <div class="form-group checkbox_form col-sm-5 col-xs-12 p-0" id="roadshow_date_required">
                <label class="label-required" for="roadshow_date">Date</label>
                <select id="roadshow_date" class="form-control" name="roadshow_date">
                    <option value="">Please choose one</option>
                </select>
                <span class="help-block label label-danger"></span>
            </div>

             <div class="form-group checkbox_form col-sm-5 col-xs-12 col-sm-offset-2 p-0" id="roadshow_time_required">
                <label class="label-required" for="roadshow_time">Time</label>
                <select id="roadshow_time" class="form-control" name="roadshow_time">
                    <option value="">Please choose one</option>
                </select>
                <span class="help-block label label-danger"></span>
            </div>

            <div class="form-group disclaimer-text">
                Terms & Conditions:
                <ol>
                    <li>Late arrivals (10 mins) will result in cancellation of appointments.</li>
                    <li>Limit to 1 customer per time slot.</li>
                    <li>We reserve the right, in our discretion to amend the terms & conditions.</li>
                </ol>
            </div>

            <div class="form-group tnc-text" id="roadshow_tnc_required">
                <div class="checkbox">
                    <label class="label-required">
                        <input id="roadshow_tnc" type="checkbox" name="roadshow_tnc" onchange="handleChange(this);" /> I have read and agreed to the <a data-toggle="modal" data-target="#consent">Consent</a>, <a class='agree-text' data-toggle="modal" data-target="#privacy_policy">Privacy Policy</a> and <a data-toggle="modal" data-target="#makeup_termsNconditions">Terms and Conditions</a></label>
                </div>
                <span class="help-block label label-danger"></span>
            </div>
            <div class='col-xs-12 submit-section'>
                <button id="submit-road-show-form" class="btn btn-default btn-block custom-submit-btn btn-lg" type="submit" onclick="this.disabled=true;submitBooking(event);">Book Now</button>
            </div>
            </form>
        </div>
    </div>
</div>
@push('modal')
<div class="modal fade" id="thankYouBooking" role="dialog" tabindex="-1" role="dialog" aria-labelledby="thank_you_booking">
    <div class="modal-dialog">
        <div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="color-text">Thank You</h3>
            </div>
            <div class="modal-body">
                <p>Thank you for your booking. We will response to you as sooner as possible.</p>
            </div>
        </div>
    </div>
</div>
@endpush
@push('js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ajaxStart(function() {
          $('.loading-container').show();
        }).ajaxStop(function() {
          $('.loading-container').hide();
        });
        $('#roadshow_artist').change(function(){
            var artist_id = $(this).val(); 
            $.ajax({
                type: 'GET',
                url: getDateUrl(),
                data: {
                   artist_id : artist_id
                },
                success: function(data){
                    $.each(data, function(k, v){
                        if( k == 'success' ){
                            $('#roadshow_date').html(v);
                        }
                    });
                }
            });
        });
        $('#roadshow_date').change(function(){
            var artist_id = $('#roadshow_artist').val(); 
            var roadshow_date = $(this).val();
            $.ajax({
                type: 'GET',
                url: getTimeUrl(),
                data: {
                   artist_id : artist_id,
                   roadshow_date : roadshow_date
                },
                success: function(data){
                    $.each(data, function(k, v){
                        if( k == 'success' ){
                            $('#roadshow_time').html(v);
                        }
                    });
                }
            });
        });            
        function getDateUrl(){
            var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
            if (iOS) {
                return "{{ env('APP_URL') }}/get-date";
            }else{
                return "/get-date";
            } 
        }
        function getTimeUrl(){
            var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
            if (iOS) {
                return "{{ env('APP_URL') }}/get-timeslot";
            }else{
                return "/get-timeslot";
            } 
        }
        function bookingUrl(){
            var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
            if (iOS) {
                return "{{ env('APP_URL') }}/submit-booking";
            }else{
                return "/submit-booking";
            }
        }
        function submitBooking(e){
            var fullname = $('#roadshow_fullname').val();
            var phone = $('#roadshow_phone').val();
            var email = $('#roadshow_email').val();
            var artist = $('#roadshow_artist').val()
            var date = $('#roadshow_date').val();
            var time = $('#roadshow_time').val();
            var tnc = $('#roadshow_tnc').prop('checked');
            e.preventDefault();
            $("span.help-block.label-danger").css('display','none');
            $.ajax({
                type: 'GET',
                url: bookingUrl(),
                data:{
                    roadshow_fullname: fullname,
                    roadshow_phone: phone,
                    roadshow_email: email,
                    roadshow_artist: artist,
                    roadshow_date: date,
                    roadshow_time: time,
                    roadshow_tnc: tnc
                },
                success: function(data){
                    $.each(data, function(k, v){
                        if( k == 'success' ){
                            if ( {{ env('LIVE') }} == 1){
                                gtag('event','booking',{
                                    event_category: 'makeup'
                                });
                            }
                            /*
                            $('#booking_makeup_form')[0].reset();
                            $("#submit-road-show-form").removeAttr('disabled');
                            $('#thankYouBooking').modal('show'); 
                            */
                            window.location.replace("{{ env('APP_URL') }}/thank-you-booking/"+v);
                        }else{
                            $("#submit-road-show-form").removeAttr('disabled');
                            $("#"+k+"_required").addClass('has-error');
                            $("#"+k+"_required span.help-block.label-danger").css('display','inline-block');
                            $("#"+k+"_required span.help-block.label-danger").html(v);
                        }
                    });
                }
            });
        }
    </script>
@endpush