<div class="road-show-detail-section p-0">
	<div class="road-show-detail-inner-section">
		<div class="road-show-detail-content">
			<h2>Join SHISEIDO<br>#INSYNCALLWAYS Roadshow<br>& Makeover Session!</h2>
			<p>Make your appointment with Shiseido Global Makeup Artist and selected Industry Makeup artists for one-on-one makeup tips in creating <span class="bold-text">multi-dimensional complexion</span></p>
			<div class="road-show-detail-datetime-location">
				<ul>
					<li>
						<img class="icon-image" src="{{ asset('images/road-show/calendar.png') }}">
						Date
						<br><span class="bold-text">1<sup>st</sup> - 7<sup>th</sup> October 2019</span>
					</li>
					<li>
						<img class="icon-image" src="{{ asset('images/road-show/time.png') }}">
						Time
						<br><span class="bold-text">11am - 8pm</span>
					</li>
					<li>
						<img class="icon-image" src="{{ asset('images/road-show/venue.png') }}">
						Venue
						<br><span class="bold-text">Center Court, Suria KLCC</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="road-show-mua-images-section">
	<h2 class="color-text">Who They Are</h2>
	<div class="row">
		<div class="col-xs-12 hidden-xs">
			<img class='img-responsive' src="{{ asset('images/road-show/mua/mua-desk-v2.png') }}">
		</div>
		<div class="col-xs-12 hidden-xs">
			<img class='img-responsive' src="{{ asset('images/road-show/mua/mua-desk-2-v2.png') }}">
		</div>
		<div class="col-xs-12 hidden-sm hidden-md hidden-lg pb-50">
			<img class='img-responsive' src="{{ asset('images/road-show/mua/mua-mob-v2.png') }}">
		</div>
		<div class="col-xs-12 hidden-sm hidden-md hidden-lg pb-50">
			<img class='img-responsive' src="{{ asset('images/road-show/mua/mua-mob-2-v2.png') }}">
		</div>
		<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
			<img class='img-responsive' src="{{ asset('images/road-show/mua/mua-mob-3-v2.png') }}">
		</div>
	</div>
</div>