@extends('layouts.app')
@section('content')
<div class="thank-you-container">
    <a href="{{ env('APP_URL') }}">
        <div class="thank-you-image">
            <div class="thank-you-image-inner">
                <img src="{{ asset('images/thank-you-page.png')}}" alt="still-valid">
                <div class="thank-you-text">
                    <p>
                    <span class="custom-spacing">Thank you for your details!</span><br>
                    Dear <span class="color-text">{{ $registration->fullname }}</span><br>
                    you may redeem your exclusive Synchro Skin Self Refreshing Sample Kit by presenting this email at your preferred Shiseido store at <span class="color-text">{{ $registration->redeem_location }}</span>.
                    </p>
                </div>
            </div>
        </div>
    </a>
</div>
@endsection
