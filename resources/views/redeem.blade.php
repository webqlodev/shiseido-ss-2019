@extends('layouts.app')
@section('content')
@php
	if( isset($is_valid) ){
		if( $is_valid == 0 ){
			$image = "images/redeem/still-valid.png";
		}else{
			$image = "images/redeem/redeemed.png";
		}	
	}else{
		abort(404);
	}
@endphp
<div id="redeem">
	@if( $is_valid == 0 )
	<div class="redeem-image">
		<img src="{{ asset($image)}}" alt="still-valid">
	</div>
	@else
	<div class="redeem-image">
		<img src="{{ asset($image)}}" alt="redeemed">
	</div>
	@endif
</div>
@endsection