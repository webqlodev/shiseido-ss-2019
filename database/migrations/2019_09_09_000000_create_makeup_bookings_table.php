<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakeUpBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('makeup_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->integer('time_slot_id');
            $table->integer('cancelled_time_slot')->default(0);
            $table->string('cancelled_email')->nullable();
            $table->string('cancelled_phone')->nullable();
            $table->integer('redeemed')->default(0);
            $table->string('image_code')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('makeup_bookings');
    }
}
