<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        User::create([
            'username' => 'webqlo',
            'email' => 'dev@webqlo.com',
            'password' => bcrypt('Webqlo!@#$'),
        ]);

        User::create([
            'username' => 'shiseido',
            'email' => 'no-reply@shiseidomy.webqlo.com',
            'password' => bcrypt('shiseido123'),
        ]);

        User::create([
            'username' => 'shiseido_bc',
            'email' => 'shiseido@webqlo.com',
            'password' => bcrypt('shiseido1234'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
