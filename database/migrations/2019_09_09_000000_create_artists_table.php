<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Artist;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Artist::create(['name' => 'Nannan Shindo']);
        Artist::create(['name' => 'Emmy Agung']);
        Artist::create(['name' => 'Kenny Yee']);
        Artist::create(['name' => 'Shiyo Joo']);
        Artist::create(['name' => 'Cat Yong']);
        Artist::create(['name' => 'Ummi Nasir']);
        Artist::create(['name' => 'Gary Cheok']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
