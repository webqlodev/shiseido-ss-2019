<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Timeslot;

class CreateTimeSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeslots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artist_id');
            $table->string('date');
            $table->string('time');
            $table->integer('status')->default(0);
            $table->timestamps();
        });

        Timeslot::create(['artist_id' => 1, 'date' => '2 Oct(Wed)', 'time' => '11:00AM - 12:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '2 Oct(Wed)', 'time' => '12:00PM - 1:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '2 Oct(Wed)', 'time' => '2:00PM - 3:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '2 Oct(Wed)', 'time' => '3:00PM - 4:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '2 Oct(Wed)', 'time' => '4:30PM - 5:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '2 Oct(Wed)', 'time' => '5:30PM - 6:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '2 Oct(Wed)', 'time' => '6:30PM - 7:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '3 Oct(Thu)', 'time' => '11:00AM - 12:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '3 Oct(Thu)', 'time' => '12:00PM - 1:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '3 Oct(Thu)', 'time' => '4:30PM - 5:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '3 Oct(Thu)', 'time' => '5:30PM - 6:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '3 Oct(Thu)', 'time' => '6:30PM - 7:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '4 Oct(Fri)', 'time' => '11:00AM - 12:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '4 Oct(Fri)', 'time' => '12:00PM - 1:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '4 Oct(Fri)', 'time' => '4:30PM - 5:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '4 Oct(Fri)', 'time' => '5:30PM - 6:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '4 Oct(Fri)', 'time' => '6:30PM - 7:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '5 Oct(Sat)', 'time' => '11:00AM - 12:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '5 Oct(Sat)', 'time' => '12:00PM - 1:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '5 Oct(Sat)', 'time' => '2:00PM - 3:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '5 Oct(Sat)', 'time' => '3:00PM - 4:00PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '5 Oct(Sat)', 'time' => '4:30PM - 5:30PM']);
        Timeslot::create(['artist_id' => 1, 'date' => '5 Oct(Sat)', 'time' => '5:30PM - 6:30PM']);
        Timeslot::create(['artist_id' => 2, 'date' => '1 Oct(Tue)', 'time' => '2:00PM - 2:30PM']);
        Timeslot::create(['artist_id' => 2, 'date' => '1 Oct(Tue)', 'time' => '2:30PM - 3:00PM']);
        Timeslot::create(['artist_id' => 2, 'date' => '1 Oct(Tue)', 'time' => '3:00PM - 3:30PM']);
        Timeslot::create(['artist_id' => 2, 'date' => '1 Oct(Tue)', 'time' => '3:30PM - 4:00PM']);
        Timeslot::create(['artist_id' => 2, 'date' => '1 Oct(Tue)', 'time' => '4:00PM - 4:30PM']);
        Timeslot::create(['artist_id' => 2, 'date' => '1 Oct(Tue)', 'time' => '4:30PM - 5:00PM']);
        Timeslot::create(['artist_id' => 2, 'date' => '1 Oct(Tue)', 'time' => '5:00PM - 5:30PM']);
        Timeslot::create(['artist_id' => 2, 'date' => '1 Oct(Tue)', 'time' => '5:30PM - 6:00PM']);
        Timeslot::create(['artist_id' => 3, 'date' => '2 Oct(Wed)', 'time' => '2:00PM - 2:30PM']);
        Timeslot::create(['artist_id' => 3, 'date' => '2 Oct(Wed)', 'time' => '2:30PM - 3:00PM']);
        Timeslot::create(['artist_id' => 3, 'date' => '2 Oct(Wed)', 'time' => '3:00PM - 3:30PM']);
        Timeslot::create(['artist_id' => 3, 'date' => '2 Oct(Wed)', 'time' => '3:30PM - 4:00PM']);
        Timeslot::create(['artist_id' => 3, 'date' => '2 Oct(Wed)', 'time' => '4:00PM - 4:30PM']);
        Timeslot::create(['artist_id' => 3, 'date' => '2 Oct(Wed)', 'time' => '4:30PM - 5:00PM']);
        Timeslot::create(['artist_id' => 3, 'date' => '2 Oct(Wed)', 'time' => '5:00PM - 5:30PM']);
        Timeslot::create(['artist_id' => 3, 'date' => '2 Oct(Wed)', 'time' => '5:30PM - 6:00PM']);
        Timeslot::create(['artist_id' => 4, 'date' => '3 Oct(Thu)', 'time' => '2:00PM - 2:30PM']);
        Timeslot::create(['artist_id' => 4, 'date' => '3 Oct(Thu)', 'time' => '2:30PM - 3:00PM']);
        Timeslot::create(['artist_id' => 4, 'date' => '3 Oct(Thu)', 'time' => '3:00PM - 3:30PM']);
        Timeslot::create(['artist_id' => 4, 'date' => '3 Oct(Thu)', 'time' => '3:30PM - 4:00PM']);
        Timeslot::create(['artist_id' => 4, 'date' => '3 Oct(Thu)', 'time' => '4:00PM - 4:30PM']);
        Timeslot::create(['artist_id' => 4, 'date' => '3 Oct(Thu)', 'time' => '4:30PM - 5:00PM']);
        Timeslot::create(['artist_id' => 4, 'date' => '3 Oct(Thu)', 'time' => '5:00PM - 5:30PM']);
        Timeslot::create(['artist_id' => 4, 'date' => '3 Oct(Thu)', 'time' => '5:30PM - 6:00PM']);
        Timeslot::create(['artist_id' => 5, 'date' => '4 Oct(Fri)', 'time' => '2:00PM - 2:30PM']);
        Timeslot::create(['artist_id' => 5, 'date' => '4 Oct(Fri)', 'time' => '2:30PM - 3:00PM']);
        Timeslot::create(['artist_id' => 5, 'date' => '4 Oct(Fri)', 'time' => '3:00PM - 3:30PM']);
        Timeslot::create(['artist_id' => 5, 'date' => '4 Oct(Fri)', 'time' => '3:30PM - 4:00PM']);
        Timeslot::create(['artist_id' => 5, 'date' => '4 Oct(Fri)', 'time' => '4:00PM - 4:30PM']);
        Timeslot::create(['artist_id' => 5, 'date' => '4 Oct(Fri)', 'time' => '4:30PM - 5:00PM']);
        Timeslot::create(['artist_id' => 5, 'date' => '4 Oct(Fri)', 'time' => '5:00PM - 5:30PM']);
        Timeslot::create(['artist_id' => 5, 'date' => '4 Oct(Fri)', 'time' => '5:30PM - 6:00PM']);
        Timeslot::create(['artist_id' => 6, 'date' => '5 Oct(Sat)', 'time' => '2:00PM - 2:30PM']);
        Timeslot::create(['artist_id' => 6, 'date' => '5 Oct(Sat)', 'time' => '2:30PM - 3:00PM']);
        Timeslot::create(['artist_id' => 6, 'date' => '5 Oct(Sat)', 'time' => '3:00PM - 3:30PM']);
        Timeslot::create(['artist_id' => 6, 'date' => '5 Oct(Sat)', 'time' => '3:30PM - 4:00PM']);
        Timeslot::create(['artist_id' => 6, 'date' => '5 Oct(Sat)', 'time' => '4:00PM - 4:30PM']);
        Timeslot::create(['artist_id' => 6, 'date' => '5 Oct(Sat)', 'time' => '4:30PM - 5:00PM']);
        Timeslot::create(['artist_id' => 6, 'date' => '5 Oct(Sat)', 'time' => '5:00PM - 5:30PM']);
        Timeslot::create(['artist_id' => 6, 'date' => '5 Oct(Sat)', 'time' => '5:30PM - 6:00PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '6 Oct(Sun)', 'time' => '2:00PM - 2:30PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '6 Oct(Sun)', 'time' => '2:30PM - 3:00PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '6 Oct(Sun)', 'time' => '3:00PM - 3:30PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '6 Oct(Sun)', 'time' => '3:30PM - 4:00PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '6 Oct(Sun)', 'time' => '4:00PM - 4:30PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '6 Oct(Sun)', 'time' => '4:30PM - 5:00PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '6 Oct(Sun)', 'time' => '5:00PM - 5:30PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '6 Oct(Sun)', 'time' => '5:30PM - 6:00PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '7 Oct(Mon)', 'time' => '2:00PM - 2:30PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '7 Oct(Mon)', 'time' => '2:30PM - 3:00PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '7 Oct(Mon)', 'time' => '3:00PM - 3:30PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '7 Oct(Mon)', 'time' => '3:30PM - 4:00PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '7 Oct(Mon)', 'time' => '4:00PM - 4:30PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '7 Oct(Mon)', 'time' => '4:30PM - 5:00PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '7 Oct(Mon)', 'time' => '5:00PM - 5:30PM']);
        Timeslot::create(['artist_id' => 7, 'date' => '7 Oct(Mon)', 'time' => '5:30PM - 6:00PM']); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeslots');
    }
}
