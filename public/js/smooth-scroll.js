// Select all links with hashes
$('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.not('[role="button"]')
.click(
    function(event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                if (iOS) {
                    var get_id = this.hash.replace("#","");
                    var element = document.getElementById(get_id);
                    console.log(element);
                    element.scrollIntoView();
                }else{
                    var offset = target.offset().top - $('#app').offset().top;
                    $('html, body').animate({
                        scrollTop: offset
                    }, 1000);
                }
            }
        }
});