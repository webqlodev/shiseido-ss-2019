$(document).ready(function(){
    var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
    $('.modal').on('hidden.bs.modal', function (e) {
        $('body').css('padding-right',0);
    });
    var owl = $(".owl-carousel");
    owl.owlCarousel({
        items:1,
        loop:true,
        autoheight:false,
        dots : false,
        nav:true
    });
    $("a.footer-social-media img").hover(
      function() {
        $( this ).attr('src','/images/footer-logo.png');
      }, function() {
        $( this ).attr('src','/images/footer-logo-white.png');
      }
    );
    $("a#logo-btn").click(function() {
        $( "a#logo-btn img" ).attr('src','/images/footer-logo.png');
    });
    $("a#logo-btn").hover(
      function() {
        $( "a#logo-btn img" ).attr('src','/images/footer-logo.png');
      }, function() {
        $( "a#logo-btn img" ).attr('src','/images/footer-logo-white.png');
      }
    );
});